package pl.sda.kantor;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

@Controller
public class ContactController {
    @GetMapping("/contact")

    public String contact(Model model){
        LocalDateTime time = LocalDateTime.now();

        DayOfWeek dayOfWeek = LocalDateTime.now().getDayOfWeek();

        String state = (dayOfWeek.equals(DayOfWeek.SATURDAY) || dayOfWeek.equals(DayOfWeek.SUNDAY))
                ?"ZAMKNIETE":"OTWARTE";

        model.addAttribute("state",state);
        model.addAttribute("time",time);
        return "contact";//to musi byc byc nazwa pliku i musi byc to w folderze templates
    }
}
