package pl.sda.kantor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity //informuje springa, ze bedzie to tabela i ten obiekt bedzie zyl w bazie danych
public class CurrencyCheck {

    @Id
    @GeneratedValue
    private Integer id;

    private long date;
    private String startCurrency;
    private String endCurrency;
    private double amount;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setStartCurrency(String startCurrency) {
        this.startCurrency = startCurrency;
    }

    public void setEndCurrency(String endCurrency) {
        this.endCurrency = endCurrency;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public long getDate() {
        return date;
    }

    public String getStartCurrency() {
        return startCurrency;
    }

    public String getEndCurrency() {
        return endCurrency;
    }

    public double getAmount() {
        return amount;
    }
}
