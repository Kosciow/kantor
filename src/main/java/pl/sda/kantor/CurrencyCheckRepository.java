package pl.sda.kantor;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CurrencyCheckRepository extends CrudRepository<CurrencyCheck, Integer> {
    long countByEndCurrency(String curency);
    long countByEndCurrencyAndAmountGreaterThan(String curency, double amount);
}
