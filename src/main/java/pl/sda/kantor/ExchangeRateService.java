package pl.sda.kantor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class ExchangeRateService {

   private RestTemplate restTemplate;
   private CurrencyCheckRepository currencyCheckRepository;
    private double spread;

    //TODO zadeklarowac Bean w KantorApplication

    public ExchangeRateService(RestTemplate restTemplate, CurrencyCheckRepository currencyCheckRepository, @Value("${spread.percent}") double spread){
        this.restTemplate = restTemplate;
        this.currencyCheckRepository=currencyCheckRepository;
        this.spread = spread;
    }

    public List<Rate> getRates() {
        ExchangeRates exchangeRates = restTemplate.getForObject("https://api.exchangeratesapi.io/latest?base=PLN", ExchangeRates.class);

        ArrayList<Rate> rates = new ArrayList<>();
        for (Map.Entry<String, Double> entry : exchangeRates.getRates().entrySet()){
            Rate rate = new Rate (entry.getKey(), entry.getValue(),spread);
            rates.add(rate);
        }

        return rates;

    }


    public String calculate(String startCurency, String endCurency, int amount) {
        String url = "https://api.exchangeratesapi.io/latest?base=" + startCurency;
        ExchangeRates exchangeRates = restTemplate.getForObject(url, ExchangeRates.class);
        for (Map.Entry<String, Double> entry : exchangeRates.getRates().entrySet()) {
            if (entry.getKey().equals(endCurency)) {
                CurrencyCheck currencyCheck = new CurrencyCheck();
                currencyCheck.setAmount((double) amount);
                currencyCheck.setEndCurrency(endCurency);
                currencyCheck.setStartCurrency(startCurency);
                currencyCheck.setDate(new Date().getTime());
                currencyCheckRepository.save(currencyCheck);
                double resalt = amount * entry.getValue();
                return String.valueOf(resalt);
            }
        }

        return null;

    }

}
