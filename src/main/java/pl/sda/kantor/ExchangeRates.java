package pl.sda.kantor;

import java.util.HashMap;
import java.util.Map;


public class ExchangeRates {

    private String base;
    private Map<String, Double> rates;

    public String getBase() {
        return base;
    }

    public Map<String, Double> getRates() {
        return rates;
    }
}
