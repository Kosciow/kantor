package pl.sda.kantor;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


public class Rate {
    private String name;
    private double rate;
    private double spread;


    public Rate(String name, double rate,double spread) {
        this.name = name;
        this.rate = rate;
        this.spread = spread;
    }

    public String getName() {
        return name;
    }

    public double getRate() {
        return rate;
    }

    public double getSellRate(){
        return rate + rate*(spread/100);
    }
    public double getBuyRate(){
        return rate - rate*(spread/100);
    }
}
