package pl.sda.kantor;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import java.util.List;

@Controller
public class RateController {

    private ExchangeRateService exchangeRateService;


    public RateController(ExchangeRateService exchangeRateService) {

        this.exchangeRateService = exchangeRateService;

    }

    private List<Rate> rates;


    @GetMapping("/rates")

    public String getRates(Model model) {
        rates = exchangeRateService.getRates();
        model.addAttribute("rates", rates);
        return "rates";
    }
    @GetMapping("/calculator")
    public String calculator(Model model){
        model.addAttribute("rates",exchangeRateService.getRates());
        return "calculator";
    }

    @PostMapping("/calculator")
    @ResponseBody
    public String calculatorResult(@RequestParam String startCurency, @RequestParam String endCurency, @RequestParam int amount){

        return "Otrzymasz: " +exchangeRateService.calculate(startCurency, endCurency, amount)+" "+endCurency;
    }
}

