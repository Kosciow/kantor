package pl.sda.kantor.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.kantor.CurrencyCheck;
import pl.sda.kantor.CurrencyCheckRepository;

@Controller
@RequestMapping("/admin")
public class AdminController {


    public CurrencyCheckRepository currencyCheckRepository;

    public AdminController(CurrencyCheckRepository currencyCheckRepository) {
        this.currencyCheckRepository = currencyCheckRepository;
    }

    @GetMapping("/conversion/stats")
    public String getStats(Model model){
        long count =currencyCheckRepository.count();
        model.addAttribute("count",count);
        long countCur=currencyCheckRepository.countByEndCurrency("EUR");
        model.addAttribute("countCur", countCur);
        long countCur1000=currencyCheckRepository.countByEndCurrencyAndAmountGreaterThan("EUR",1000);
        model.addAttribute("countCur1000", countCur1000);


        return "adminStats";
    }
}
